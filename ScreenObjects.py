import pygame
from abc import ABC, abstractmethod
import sys

PATH = sys.path[0].replace('\\','/')+'/'

class GuiObj():

    

    def __init__(self, filepath:str=None): # CONSTRUCTOR FUNCTION

        try:
            self.__image = pygame.image.load(PATH+filepath) #  IMAGE STORES THE BUTTON'S APPEARANCE
            self.__rect = self.__image.get_rect()  #  RECT STORES THE SIZE/BOUNDARIES OF THE IMAGE
        except:
            self.__image = None
            self.__rect = None
        self.__enabled = True
        self.__pos = (0,0)  # POS STORES ITS LOCATION ON SCREEN WHEN VISIBLE
        self.__highlightColour = (0,0,255)
        self.__window : Window = None
        self.__mouseListener:MouseListener = None

    def setHighlightColour(self,colour:tuple[int,int,int]):
        self.__highlightColour = colour
        self.createHighlight()

    def createHighlight(self):
        (x,y) = self.__image.get_rect().bottomright
        z = pygame.Surface((x,y))
        z.fill(self.__highlightColour)
        y = pygame.Surface((x-4,y-4))
        y.fill((74,65,42))
        z.blit(y,(2,2))
        z.set_colorkey((74,65,42))
        self.highlightImg = z

    def highlight(self):
        self.__normImg = self.getImage().__copy__()
        temp = self.getImage()
        temp.blit(self.highlightImg,(0,0))
        self.setImage(temp)
        self.getWindow().updateDisplay()

    def lowlight(self):
        self.setImage(self.__normImg)
        self.getWindow().updateDisplay()

    def scaleImage(self,factor:float):
        self.setImage(pygame.transform.scale_by(self.__image,factor))
        

    def addMouseListener(self,mouseListener:"MouseListener"):
        self.__mouseListener = mouseListener
    def removeMouseListener(self):
        self.__mouseListener = None

    ###################### SETTERS/GETTERS ##############################
    def setImage(self, image:pygame.Surface):
        self.__image = image
        self.setRect()
        self.createHighlight()
    def loadImage(self, filepath):
        try:
            self.__image = pygame.image.load(filepath) #  IMAGE STORES THE BUTTON'S APPEARANCE
            self.setRect()  #  RECT STORES THE SIZE/BOUNDARIES OF THE IMAGE
            
        except:
            ''
    def setEnabled(self,boole:bool):
        self.__enabled=boole
    def setPos(self,pos:tuple[int,int]):
        self.__pos = pos
        self.setRect()
    def setWindow(self,window:"Window"):
        self.__window = window
    def getWindow(self) -> "Window":
        return self.__window
    
    def setRect(self):
        rect = self.__image.get_rect()
        self.__rect = pygame.Rect(self.__pos,rect.bottomright)
    def getImage(self) -> pygame.Surface:
        return self.__image
    def getRect(self) -> pygame.Rect:
        return self.__rect
    def getPos(self) -> tuple[int,int]:
        return self.__pos
    def isEnabled(self) -> bool:
        return self.__enabled
    
    

    def onClick(self, event:pygame.event.Event):
        if self.__mouseListener:
            self.__mouseListener.onClick(event,self)
    def mouseEntered(self, event:pygame.event.Event):
        if self.__mouseListener:
            self.__mouseListener.mouseEntered(event,self)
    def mouseExited(self, event:pygame.event.Event):
        if self.__mouseListener:
            self.__mouseListener.mouseExited(event,self)


class Window():

    def __init__(self, x:int, y:int,fps:int,bgc:tuple[int,int,int]=(0,0,0),screen:pygame.Surface=None) -> None:
        self.__objectsOnScreen = list[GuiObj]()
        if screen:
            self.__screen = screen
        else:
            self.__screen = pygame.display.set_mode((x, y))
        self.__eventListner = EventListener(self,fps)
        self.__bgc = bgc
        self.sleeping = False

    def pygameDisplayUpdate(self):
        if not self.sleeping:
            pygame.display.update()

    
    def getWindow(self) -> pygame.Surface:
        return self.__screen
        
    def switchToEvents(self):
        self.__eventListner.switchToEvents()

    def setBGColour(self,r:int,g:int,b:int):
        self.__bgc = (r,g,b)
        
    def clear(self):
        self.__screen.fill(self.__bgc)
        self.__objectsOnScreen = list[GuiObj]()
        

    def updateDisplay(self):
        self.__screen.fill(self.__bgc)
        temp = self.__objectsOnScreen.copy()
        self.clear()
        for obj in temp:
            self.addToDisplay(obj,obj.getPos(),False)
        self.pygameDisplayUpdate()

    def removeFromDisplay(self,target:GuiObj,update:bool=True):
        i = 0
        for obj in self.__objectsOnScreen:
            if obj == target:
                self.__objectsOnScreen.pop(i)
            i+=1
        if update:
            self.updateDisplay()
    
    def objectClicked(self,pos) -> GuiObj:     # RETURNS THE BUTTON YOU CLICKED ON, OR NONE IF YOU CLICKED NOTHING
        tempObject = None
        for object in self.__objectsOnScreen:
            if object.getRect().contains(pos,(0,0)):
                tempObject = object
        return tempObject
    

    def addToDisplay(self,object:GuiObj, pos:tuple[int,int],update:bool=True):    # DISPLAYS THE GIVEN BUTTON/CARD ON SCREEN, THEN ADDS IT TO THE LIST OF THINGS ON SCREEN
        
        object.setPos(pos)
        object.setWindow(self)
        self.__screen.blit(object.getImage(), pos)
        self.__objectsOnScreen.append(object)
        
        if update and not self.sleeping:
            pygame.display.update(object.getRect())

    def sleep(self,seconds:int):
        self.__sleeping = True
        self.__eventListner.sleep(seconds)


    ############ OVERRIDE THESE ################
    def keyPressed(self,event:pygame.event.Event):
        pass

    def calledEachTick(self):
        pass

    def onClose(self):
        pass

class EventListener():

    def __init__(self,window:Window,fps:int=30) -> None:
        self.fps = fps
        self.__window = window
        self.__sleepTicks = 0
        self.__sleeping = False

    def sleep(self, seconds:int):
        self.__sleepTicks = seconds*self.fps
        self.__sleeping = True
    def switchToEvents(self):
        clock = pygame.time.Clock()
        oldObj = GuiObj()
        while True:
            clock.tick(self.fps) 
            if self.__sleeping:
                self.__sleepTicks -=1
                self.__sleeping = bool(self.__sleepTicks != 0)
                self.__window.sleeping = self.__sleeping
            self.__window.calledEachTick()
            for event in pygame.event.get():
                if event.type == pygame.QUIT: #   THIS ALLOWS YOU TO CLOSE THE WINDOW
                    print('why would i do what you want')
                    self.__window.onClose()
                    pygame.quit()
                    exit()
                elif event.type == pygame.MOUSEMOTION and not self.__sleeping:
                    enteredObj = self.__window.objectClicked(event.pos)
                    
                    if oldObj != enteredObj:
                        if oldObj:
                            oldObj.mouseExited(event)
                        oldObj = enteredObj
                        if enteredObj:
                            enteredObj.mouseEntered(event)


                elif event.type == pygame.MOUSEBUTTONDOWN and not self.__sleeping:  

                    obj = self.__window.objectClicked(event.pos)
                    if obj:
                        obj.onClick(event)
                elif event.type == pygame.KEYDOWN and not self.__sleeping:
                    self.__window.keyPressed(event)

class mouseClickEvent(ABC):
    pos:tuple[int,int]
    button:int 
class mouseMotionEvent(ABC):
    pos:tuple[int,int]
    rel:tuple[int,int]
    

class MouseListener(ABC):

    def setObj(self,obj:GuiObj):
        self.__obj = obj
    def getObj(self) -> GuiObj:
        return self.__obj

    @abstractmethod
    def onClick(self,event:mouseClickEvent,obj:GuiObj):
        pass

    def mouseEntered(self,event:mouseMotionEvent,obj:GuiObj):
        if obj.isEnabled():
            obj.highlight()
  
    def mouseExited(self,event:mouseMotionEvent,obj:GuiObj):
        if obj.isEnabled():
            obj.lowlight()
                

""" 
QUIT             none
ACTIVEEVENT      gain, state
KEYDOWN          unicode, key, mod
KEYUP            key, mod
MOUSEMOTION      pos, rel, buttons           Event types and their attributes
MOUSEBUTTONUP    pos, button
MOUSEBUTTONDOWN  pos, button                          
JOYAXISMOTION    joy, axis, value
JOYBALLMOTION    joy, ball, rel
JOYHATMOTION     joy, hat, value
JOYBUTTONUP      joy, button
JOYBUTTONDOWN    joy, button
VIDEORESIZE      size, w, h
VIDEOEXPOSE      none
USEREVENT        code
 """
    