from TricksAndTrumps import *
import time
import traceback


class MainMenu(ScreenObjects.Window):

    class offlineMouseListener(ScreenObjects.MouseListener):
        def onClick(self, event: ScreenObjects.mouseClickEvent, obj: ScreenObjects.GuiObj):
            if obj.isEnabled():
                if event.button == pygame.BUTTON_LEFT:
                    game = Game()
                    game.start()
                    game.window.switchToEvents()

    class onlineMouseListener(ScreenObjects.MouseListener):            
        def onClick(self, event: ScreenObjects.mouseClickEvent, obj: ScreenObjects.GuiObj):
            if obj.isEnabled():
                if event.button == pygame.BUTTON_LEFT:
                    try:
                        server = Server('149.102.132.224',4657)
                    except:
                        server = Server('149.102.132.224',4658)
                    window:MainMenu = obj.getWindow()
                    window.setServer(server)
                    if server.playerCount != 2:
                        
                        window.clear()
                        text = Text('sans-serif',40,(0,0,0),"Waiting For Another Player...")
                        window.addToDisplay(text,(400,315),False)
                        window.updateDisplay()



    def __init__(self) -> None:
        super().__init__(1200, 650,30,(52,104,104))
        self.server = None
        title = Text('sans-serif',50,(0,0,0),'Tricks and Trumps Bidding')
        offline = Text('sans-serif',40,(0,0,0),' Play Offline ',(255,255,255))
        
        offline.addMouseListener(MainMenu.offlineMouseListener())
        online = Text('sans-serif',40,(0,0,0),' Play Online ',(255,255,255))
        online.addMouseListener(MainMenu.onlineMouseListener())
        
        self.clear()
        
        self.addToDisplay(title,(300,100),False)
        self.addToDisplay(offline,(300,500),False)
        self.addToDisplay(online,(700,500),False)
        self.pygameDisplayUpdate()
        
        


    def setServer(self,server:Server):
        self.server = server
    
    def calledEachTick(self):
        if self.server:
            if self.server.playerCount == 2:
                game = Game(self.server)
                game.start()
                game.window.switchToEvents()
            else:
                change = self.server.getChange()
                if change.get('change') == Server.PLAYERJOINED:
                    self.server.playerCount +=1
    def onClose(self):
        if self.server:
            self.server.shutdown()
            
            


pygame.init()
menu = MainMenu()
try:
    menu.switchToEvents()
except Exception:
    if menu.server:
        menu.server.shutdown()
    print(traceback.format_exc())
    input()

    

