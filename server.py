from socket import socket
import NetworkClasses
import json
import random


changeList = list[dict]()


def checkGameState(index:int):
    
    try:
        change = changeList[index]
        response = change
    except:
        response ={}
    return response

class Deck():
    SUITS = ['spades','clubs','hearts','diamonds']
    SYMBOLS = ['2','3','4','5','6','7','8','9','10','jack','queen','king','ace']
    COLOURS = ['red','black']

    def __init__(self, joker:bool=False) -> None:
        
        self.cards = list()
        for suit in Deck.SUITS:
            
            for symbol in Deck.SYMBOLS:
                    self.cards.append((suit,symbol))
                
        if joker:
            self.cards.append(('joker','red'))
            self.cards.append(('joker','black'))

        self.shuffled = self.shuffle()
    
    def shuffle(self):
        self.accessed = 0
        shuffled = list()
        size = len(self.cards)
        copy = self.cards.copy()
        for i in range(size):
            index = random.randint(0,size-1-i)
            shuffled.append(copy[index])
            copy.pop(index)
        return shuffled
    
    def getShuffled(self):
        self.accessed +=1
        temp = self.shuffled
        if self.accessed == Lobby.maxPlayers:
            self.shuffled = self.shuffle()
        return temp



class Lobby():
    players = dict[socket:int]()
    maxPlayers = 2

    def playerJoined(client:socket) -> int:
        playerNum = len(Lobby.players)
        if playerNum == Lobby.maxPlayers:
            return -1
        while Lobby.players.get(playerNum):
            playerNum-=1
        Lobby.players.update({client:playerNum})
        changeList.append({'change':2,'playerNum':playerNum})
        return playerNum
    
    def playerLeft(client:socket):
        changeList.append({'change':3,'playerNum':Lobby.players[client]})
        Lobby.players.pop(client)
        if len(Lobby.players) == 0:
            changeList.clear()
        
        


class Server(NetworkClasses.ServerSide):

    deck = Deck()

    def onReceive(self, client: socket, msg: str):
        if msg:

            if msg == "getDeck":
                self.send(client,Server.deck.getShuffled())
            else:

                try:
                    index = int(msg)
                    response = checkGameState(index)
                    self.send(client,response)
                except:
                    change = json.loads(msg)
                    changeList.append(change)
                    self.send(client,{"Successful":True})
 
        

    def onConnect(self, client: socket):
        playerNum = Lobby.playerJoined(client)
        currentState = {'nextChange':len(changeList),'playerNum':playerNum,'playerCount':len(Lobby.players)}
        self.send(client,currentState)
    
    def onDisconnect(self, client: socket):
        Lobby.playerLeft(client)
    
    def send(self, client: socket, msg: dict):
        return super().send(client, json.dumps(msg))
    
    
try:  
    server = Server(port=4657)
except:
    server = Server(port=4658)
try:
    server.listen()
except KeyboardInterrupt:
    server.shutdown()
    print("\n[SERVER STOPPED]",flush=True)







