import pygame
from pygame.event import Event
import ScreenObjects
import random
import NetworkClasses
import json



""" 
QUIT             none
ACTIVEEVENT      gain, state
KEYDOWN          unicode, key, mod
KEYUP            key, mod
MOUSEMOTION      pos, rel, buttons           Event types and their attributes
MOUSEBUTTONUP    pos, button
MOUSEBUTTONDOWN  pos, button                          
JOYAXISMOTION    joy, axis, value
JOYBALLMOTION    joy, ball, rel
JOYHATMOTION     joy, hat, value
JOYBUTTONUP      joy, button
JOYBUTTONDOWN    joy, button
VIDEORESIZE      size, w, h
VIDEOEXPOSE      none
USEREVENT        code
 """

class GameWindow(ScreenObjects.Window):

    bgc = (52,104,104)

    def __init__(self,game:"Game") -> None:
        super().__init__(1200, 650,30,(52,104,104))
        self.clear()
  
        self.__game = game
        

    def getGame(self) -> "Game":
        return self.__game

    def keyPressed(self, event: Event):
        pass
    def calledEachTick(self):
        self.__game.calledEachTick()
    def onClose(self):
        if self.__game.server:
            self.__game.server.shutdown()

    
class Card(ScreenObjects.GuiObj):


    height :int = 0
    width :int = 0
    

    def __init__(self, symbol:str,suit:str):
        if symbol not in Deck.SYMBOLS+['joker']:
            print("symbol must be one of the following: ", Deck.SYMBOLS)
            exit()
        elif suit not in Deck.SUITS:
            if suit in Deck.COLOURS and symbol == 'joker':
                super().__init__(rf"cardImgs/{suit}_{symbol}.png")
            else:
                print("suit must be one of the following:",)
                exit()
        else:
            super().__init__(rf"cardImgs/{symbol}_of_{suit}.png")
        
        self.__back = pygame.transform.scale_by(pygame.image.load(ScreenObjects.PATH+r"cardImgs/back.png"),0.15)
        self.scaleImage(0.15)
        Card.height = self.getImage().get_height()
        Card.width = self.getImage().get_width()
        self.__symbol = symbol
        self.__suit = suit
        
        
    def getImage(self) -> pygame.Surface:
        if self.isEnabled():
            return super().getImage()
        else:
            return self.__back


    def setCollection(self,collection:"CardCollection"):
        self.__collection = collection
    def getCollection(self) -> "CardCollection":
        return self.__collection
    
    def moveTo(self, collection:"CardCollection"):
        self.__collection.removeCard(self)
        self.setCollection(collection)
        collection.addCard(self)

    def getSuit(self) ->str:
        return self.__suit
    def getSymbol(self) ->str:
        return self.__symbol

    def isSuit(self,suit:str) -> bool:
        return bool(self.__suit == suit)
    def isSymbol(self, symbol:str) -> bool:
        return bool(self.__symbol==symbol)
    def value(self) -> int:
        try:
            val = int(self.__symbol)
        except:
            if self.__symbol == 'jack':
                val = 11
            elif self.__symbol == 'queen':
                val = 12
            elif self.__symbol == 'king':
                val = 13
            elif self.__symbol == 'ace':
                val = 14
        return val
    
    
    

class Text(ScreenObjects.GuiObj):
    def __init__(self,font:str,size:int,colour:tuple[int,int,int],text:str="",bgc:tuple[int,int,int]=GameWindow.bgc):
        super().__init__("")
        self.__font = pygame.font.SysFont(font,size)
        self.__text = text
        self.__colour = colour
        self.__bgc = bgc
        self.refreshImg()
        
    def setBGC(self,colour:tuple[int,int,int]):
        self.__bgc = colour
        self.refreshImg()
    
    def setTextColour(self,colour:tuple[int,int,int]):
        self.__colour = colour
        self.refreshImg()
    
    def setText(self,text:str):
        self.__text = text
        self.refreshImg()
    
    def refreshImg(self):
        self.setImage(self.__font.render(self.__text,True,self.__colour,self.__bgc))
        if self.getWindow():
            self.getWindow().updateDisplay()

    def getText(self) -> str:
        return self.__text
        

class CardCollection(list[Card]):

    cardCollections:list["CardCollection"] = list()

    def __init__(self):
        self.__pos = len(CardCollection.cardCollections)
        CardCollection.cardCollections.append(self)
        self.__playedCard:Card = None
    def getPos(self) -> int:
        return self.__pos
    
    def addCard(self,card:Card):
        card.setCollection(self)
        self.append(card)

    def removeCard(self,card:Card):
        i = 0
        self.__playedCard = card
        for search in self:
            if search == card:
                self.pop(i)
                if search.getWindow():
                    search.getWindow().removeFromDisplay(search)
                break
            i+=1

    def shuffle(self):
        temp = self.copy()
        self.clear()
        for i in range(len(temp)):
            index = random.randint(0,len(temp)-1)
            self.append(temp[index])
            temp.pop(index)

    def containsSuit(self,suit:str):
        for card in self:
            if card.getSuit() == suit:
                return True
        return False
    

    def getTopCard(self) -> Card:
        return self[-1]
    def getPlayedCard(self) -> Card:
        return self.__playedCard
    

class Deck(ScreenObjects.GuiObj):
    SUITS = ['spades','clubs','hearts','diamonds']
    SYMBOLS = ['2','3','4','5','6','7','8','9','10','jack','queen','king','ace']
    COLOURS = ['red','black']
    cardDict:dict[dict[Card]] = dict[dict[Card]]() 

    def __init__(self, joker:bool=False) -> None:
        super().__init__(r"cardImgs/deck.png")
        
        self.scaleImage(0.15)
        self.cards = CardCollection()
        for suit in Deck.SUITS:
            Deck.cardDict.update({suit:{}})
            for symbol in Deck.SYMBOLS:
                card = Card(symbol,suit)
                Deck.cardDict[suit].update({symbol:card})
                self.cards.addCard(card)
        if joker:
            self.cards.addCard(Card('joker','red'))
            self.cards.addCard(Card('joker','black'))
    
    def shuffle(self):
        self.cards.shuffle()

    def setOrder(self,deckOrder:list[tuple[str,str]]):
        self.cards.clear()
        for card in deckOrder:
            self.cards.addCard(Deck.cardDict[card[0]][card[1]])
        
    

class Pile(ScreenObjects.GuiObj):
    def __init__(self):
        super().__init__("")
        self.cards = CardCollection()
        self.__topCard = None
        self.setImage()
    
    def clear(self):
        self.cards.clear()
        self.setImage()

    def setImage(self):
        
        image = pygame.Surface((Card.width+50,Card.height))
        image.fill(GameWindow.bgc)

        if len(self.cards) != 0:
            self.__topCard = self.cards.getTopCard()
            x = 0
            for i in range(3):
                if 3-i <= len(self.cards):
                    image.blit(self.cards[i-3].getImage(),(0+x*25,0))
                    x+=1

        super().setImage(image)
    
    def getImage(self) -> pygame.Surface:
        if (len(self.cards)) !=0:
            if self.__topCard != self.cards.getTopCard():
                self.setImage()
        return super().getImage()
    
    


class Hand(CardCollection):

    class MouseListenerHand(ScreenObjects.MouseListener):
        def onClick(self, event: ScreenObjects.mouseClickEvent, obj: ScreenObjects.GuiObj):
            if obj.isEnabled():
                if event.button == pygame.BUTTON_LEFT:
                    window:GameWindow = obj.getWindow()
                    window.getGame().cardClicked(obj)
        

    ######################################################################################################
    
    mouseListener:"Hand.MouseListenerHand"

    def __init__(self, player:"Player",game:"Game"):
        self.__player = player
        Hand.mouseListener = Hand.MouseListenerHand()
        self.__game = game
    def display(self, y:int):
        self.__y = y
        width = self.__game.window.getWindow().get_width()
        cardWidth = Card.width
        totalCardsWidth = (cardWidth/2)*(len(self)+1)
        start = (width - totalCardsWidth)/2

        for card in self:
            self.__game.window.addToDisplay(card,(start,y),False)
            start+=cardWidth/2

    def updateDisplay(self):
        width = self.__game.window.getWindow().get_width()
        cardWidth = Card.width
        totalCardsWidth = (cardWidth/2)*(len(self)+1)
        start = (width - totalCardsWidth)/2

        for card in self:
            card.setPos((start,self.__y))
            start+=cardWidth/2

    def getPlayer(self) -> "Player":
        return self.__player
    
    def addCard(self, card: Card):
        if self.__player != self.__game.localPlayer:
            card.setEnabled(False)
        else:
            card.setEnabled(True)
            card.addMouseListener(Hand.mouseListener)
        super().addCard(card)
    def removeCard(self, card: Card):
        card.removeMouseListener()
        card.setEnabled(True)
        super().removeCard(card)
        self.updateDisplay()
        

class Player():
    def __init__(self,playerNum:int,game:"Game") -> None:
        self.__hand = Hand(self,game)
        self.NUM = playerNum
        self.__bid = 0
        self.__tricks = 0
        self.__score = 0
        self.__game = game
        self.__trickText = Text("sans-serif",40,(0,0,0),f"Tricks Won: {self.__tricks}")
        self.__bidText = Text("sans-serif",40,(0,0,0),f"Tricks Bid: {self.__bid}")
        self.__scoreText = Text("sans-serif",40,(0,0,0),f"Current Score: {self.__score}")

    def newGame(self):
        self.__hand = Hand(self,self.__game)
        self.__bid = 0
        self.__tricks = 0
        self.__trickText = Text("sans-serif",40,(0,0,0),f"Tricks Won: {self.__tricks}")
        self.__bidText = Text("sans-serif",40,(0,0,0),f"Tricks Bid: {self.__bid}")
        self.__scoreText = Text("sans-serif",40,(0,0,0),f"Current Score: {self.__score}")


    def displayInfo(self,y:int):
        self.__game.window.addToDisplay(self.__scoreText,(10,y),False)
        self.__game.window.addToDisplay(self.__bidText,(10,y+30),False)
        self.__game.window.addToDisplay(self.__trickText,(10,y+60),False)


    def addScore(self,points:int):
        self.__score += points
        self.updateScoreText()
    def getScore(self) -> int:
        return self.__score

    def trickWon(self):
        self.__tricks += 1
        self.updateTrickText()
    
    def getTricks(self) -> int:
        return self.__tricks
    
    def getHand(self) -> Hand:
        return self.__hand

    def setBid(self,bid:int):
        self.__bid = bid
        self.updateBidText()

    def madeBid(self,bid:int):
        self.__game.gamestateChanged({'change':Server.MADEBID,'bid':bid,'playerNum':self.NUM})


    def getBid(self) -> int:
        return self.__bid
    
    def getGame(self) -> "Game":
        return self.__game
    
    def updateTrickText(self):
        self.__trickText.setText(f"Tricks Won: {self.__tricks}")
    def updateBidText(self):
        self.__bidText.setText(f"Tricks Bid: {self.__bid}")
    def updateScoreText(self):
        self.__scoreText.setText(f"Current Score: {self.__score}")

class CPU(Player):
    def play(self):
        if self.getGame().bidding:
            if self.getGame().restrictedBid == 5:
                self.madeBid(4)
            else:
                self.madeBid(5)
        else:
            card = self.getGame().playableCards(self.getHand())[0]
            self.getGame().moveCard(card,self.getGame().pile.cards)


class bidMouseListener(ScreenObjects.MouseListener):
    def onClick(self, event: ScreenObjects.mouseClickEvent, obj: Text):
        
        temp:GameWindow = obj.getWindow()
        temp.getGame().localPlayer.madeBid(int(obj.getText()))


class Game():
    
    def __init__(self,server:"Server"=None) -> None:
        self.server = server
        if server:
            
            localNum = server.playerNum
        else:
            localNum = 0

        self.window = GameWindow(self)
        self.deck = Deck()
        self.pile = Pile()
        self.players = list[Player]()

        for i in range(2):
            if server or i == localNum:
                self.players.append(Player(i,self))
            else:
                self.players.append(CPU(i,self))
        self.localPlayer:Player = self.players[localNum]

        self.startTurn = 0
        self.turn = -1
        self.turnCount = 0
        self.rounds = 10
        self.roundCount = 0
        self.bidding = False
        self.biddingDisplayed = False
        self.bidsMade = 0
        self.restrictedBid = -1
        self.__counter = 0
        self.trumps = ''
        self.turnText = None
        self.nextTurn(True)


    def start(self):
        
        if self.server:
            self.server.send("getDeck")
            deckOrder = self.server.recv()
            self.deck.setOrder(deckOrder)
        else:
            self.deck.shuffle()
        for i in range(self.rounds):
            for j in range(len(self.players)):
                self.deck.cards.getTopCard().moveTo(self.players[j].getHand())
            

        self.players[1-self.localPlayer.NUM].getHand().display(50)
        self.localPlayer.getHand().display(519)
        self.window.addToDisplay(self.deck,(400,300),False)
        self.window.addToDisplay(self.deck.cards.getTopCard(),(410,300),False)
        
        self.players[self.localPlayer.NUM].displayInfo(550)
        self.players[1-self.localPlayer.NUM].displayInfo(10)
        self.window.addToDisplay(self.pile,(600,300),False)
        self.trumps = self.deck.cards.getTopCard().getSuit()
        self.bidding = True
        

        

    def cardClicked(self,card:Card):
        if self.turn == self.localPlayer.NUM and not self.bidding:
            if card.getCollection() == self.localPlayer.getHand():
                if card in self.playableCards(card.getCollection()):
                    self.moveCard(card,self.pile.cards)

    def nextTurn(self,ignoreRound:bool=False):
        self.turn += 1
        if not ignoreRound:
            self.turnCount += 1
        if self.turn == len(self.players):
            self.turn = 0

        self.window.removeFromDisplay(self.turnText,False)
        if self.turn == self.localPlayer.NUM:
            text = Text('sans-serif',30,(0,0,0),"Your Turn")
        else:
            text = Text('sans-serif',30,(0,0,0),"Opponent's Turn")
        self.turnText = text
        self.window.addToDisplay(text,(50,320),False)  

        if self.turnCount == len(self.players):
            self.turnCount = 0
            self.nextRound()
    
    def nextRound(self):
        max : int = 0
        cardVals = list[int]()
        startSuit = self.pile.cards[0].getSuit()
        for player in self.players:
            cardPlayed = player.getHand().getPlayedCard()
            cardVal = cardPlayed.value()
            if cardPlayed.getSuit() == self.trumps:
                cardVal +=100
            elif cardPlayed.getSuit() != startSuit:
                cardVal = 0
            cardVals.append(cardVal)
            if cardVals[player.NUM] > cardVals[max]:
                max = player.NUM
        winner = self.players[max]
        winner.trickWon()
        self.turn = winner.NUM
        self.roundCount += 1

        if winner == self.localPlayer:
            temp = 'won'
        else:
            temp = 'lost'
        text = Text('sans-serif',40,(0,0,0),f"You {temp} this trick")
        self.pile.clear()
        self.window.addToDisplay(text,(400,440))
        self.window.sleep(3)
        self.window.removeFromDisplay(text,False)
        
        if self.roundCount == self.rounds:
            self.nextGame()

    def nextGame(self):
        self.window.clear()
        self.startTurn += 1
        if self.startTurn == len(self.players):
            self.startTurn = 0
        self.turn = self.startTurn
        self.turnCount = 0
        self.roundCount = 0
        self.biddingDisplayed = False
        self.bidsMade = 0
        self.restrictedBid = -1
        self.trumps = ''


        self.deck = Deck()
        self.pile = Pile()
        for player in self.players:
            if player.getTricks() == player.getBid():
                player.addScore(10)
            player.addScore(player.getTricks())
            player.newGame()
        self.start()
    
    def getBid(self):
        self.biddingDisplayed = True
        prompt = Text('sans-serif',30,(0,0,0),"How many tricks do you think you will win?")
        self.bidOptions = list[Text]()
        x = 140
        y = 470
        for i in range(11):
            self.bidOptions.append(Text("sans-serif",40,(0,0,0),' '+str(i)+' ',(255,255,255)))
            if i != self.restrictedBid:
                self.bidOptions[i].addMouseListener(bidMouseListener())
                self.window.addToDisplay(self.bidOptions[i],(x,y),False)
                x+=90
            
                
        self.window.addToDisplay(prompt,(400,440),False)
        self.bidOptions.append(prompt)
    


   
    def gamestateChanged(self,change:dict,send:bool=True):
        
        if self.server and send:
            self.server.send(json.dumps(change))
            response = self.server.recv()
            print(response)
            self.server.nextChange += 1


        if change['change'] == Server.CARDMOVED:
            card:Card = Deck.cardDict[change['suit']][change['symbol']]
            card.moveTo(CardCollection.cardCollections[change['collection']])
            self.nextTurn()

        elif change['change'] == Server.MADEBID:
            self.players[change['playerNum']].setBid(change['bid'])
            self.bidsMade +=1
            if self.bidsMade == len(self.players)-1:
                x = 10
                for player in self.players:
                    x -= player.getBid()
                self.restrictedBid = x 

            if self.turn == self.localPlayer.NUM:
                for obj in self.bidOptions:
                    self.window.removeFromDisplay(obj,False)
            self.nextTurn(True)

        elif change['change'] == Server.PLAYERLEFT:
            self.window.clear()
            msg = Text('sans-serif',40,(0,0,0),"Your opponent left the game")
            self.window.addToDisplay(msg,(400,315))
            self.window.updateDisplay()
            pygame.time.delay(5000)
            self.server.shutdown()
            pygame.quit()
            exit()
            
            
            


    def moveCard(self,card:Card,collection:CardCollection):
        change = {
            'change':Server.CARDMOVED,
            'suit': card.getSuit(),
            'symbol':card.getSymbol(),
            'collection':collection.getPos()     
                  }
        self.gamestateChanged(change)
        

    def playableCards(self,collection:CardCollection) -> list[Card]:
        if len(self.pile.cards) == 0 or not collection.containsSuit(self.pile.cards.getTopCard().getSuit()):
            return collection
        else:
            playable = list[Card]()
            for card in collection:
                if card.getSuit() == self.pile.cards.getTopCard().getSuit():
                    playable.append(card)
            return playable

    

    def calledEachTick(self):
        self.__counter += 1
        if not self.__counter%5:
            self.window.updateDisplay()
        if self.__counter == 30:
            self.__counter = 0
            if self.server:
                change = self.server.getChange()
                if change:
                    self.gamestateChanged(change,False)

        if self.bidding:
            if self.turn == self.localPlayer.NUM and not self.biddingDisplayed:
                self.getBid()
        if self.bidsMade == len(self.players):
            self.bidding = False
        if isinstance(self.players[self.turn],CPU):
            cpu:CPU = self.players[self.turn]
            cpu.play()

class Server(NetworkClasses.ClientSide):

    CARDMOVED = 0
    MADEBID = 1
    PLAYERJOINED = 2
    PLAYERLEFT = 3

    def __init__(self, server: str, port: int) -> None:
        super().__init__(server, port)
        response = self.recv()
        self.playerNum = response['playerNum']
        self.playerCount = response['playerCount']
        self.nextChange = response['nextChange']
    
    def getChange(self) -> dict:
        self.send(str(self.nextChange))
        response = self.recv()
        if response:
            self.nextChange+=1
        return response

    def recv(self) -> dict:
        msg = super().recv()
        try:
            return json.loads(msg)
        except Exception as e:
            print(e)
            print(msg)
            exit()
    


            

            

